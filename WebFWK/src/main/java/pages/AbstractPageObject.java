package pages;

import driver.WebFWK;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Configuration;

public abstract class AbstractPageObject {

  protected WebFWK driver;
  protected Configuration config;
  protected Logger log = LogManager.getLogger(WebFWK.class.getName());
  protected int longWait;
  protected int shortWait;
  private WebDriverWait wait;

  /**
   * Constructor.
   *
   * @param driver
   */
  public AbstractPageObject(WebFWK driver) {
    this.driver = driver;
    PageFactory.initElements(this.driver.getDriver(), this);
    config = new Configuration();
    longWait = config.getLongWebDriverWait();
    shortWait = config.getShortWebDriverWait();
  }

  /**
   * Espera seg segundos.
   *
   * @param seg
   */
  public void wait(int seg) {
    try {
      Thread.sleep(1000 * seg);
    } catch (InterruptedException e) {
      log.error("Hilo principal interrumpido" + e);
    }
  }

  /**
   * Espera seg segundos a que element sea visible.
   *
   * @param element elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return devuelve element si es visible, null en caso contrario
   */
  public WebElement wait(WebElement element, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      return wait.until(ExpectedConditions.visibilityOf(element));

    } catch (ElementNotVisibleException e) {
      log.error(
          "TimeOut. Elemento " + element + " no visible después de " + seg + " segundos de espera");
      return null;
    }
  }

  /**
   * Espera seg segundos a que element sea visible.
   *
   * @param by localizador del elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return devuelve element si es visible, null en caso contrario
   */
  public WebElement wait(By by, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      return wait.until(ExpectedConditions.visibilityOfElementLocated(by));

    } catch (ElementNotVisibleException e) {
      log.error(
          "TimeOut. Elemento " + by + " no visible después de " + seg + " segundos de espera");
      return null;
    }
  }

  /**
   * Espera seg segundos a que element sea invisible.
   *
   * @param element elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return true si element desaparece, false si sigue visible
   */
  public boolean waitInvisible(WebElement element, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      return wait.until(ExpectedConditions.invisibilityOf(element));

    } catch (TimeoutException e) {
      log.error(
          "TimeOut. Elemento "
              + element
              + " aún visible después de "
              + seg
              + " segundos de espera");
      return false;
    }
  }

  /**
   * Espera seg segundos a que element sea invisible.
   *
   * @param by localizador del elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return true si element desaparece, false si sigue visible
   */
  public boolean waitInvisible(By by, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));

    } catch (TimeoutException e) {
      log.error(
          "TimeOut. Elemento " + by + " aún visible después de " + seg + " segundos de espera");
      return false;
    }
  }

  /**
   * Espera seg segundos a que element sea clicable.
   *
   * @param element elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return devuelve element si es clicable, null en caso contrario
   */
  public WebElement waitForElementClickable(WebElement element, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      wait(element, seg);
      return wait.until(ExpectedConditions.elementToBeClickable(element));

    } catch (TimeoutException e) {
      log.error(
          "TimeOut. Elemento "
              + element
              + " no seleccionable después de "
              + seg
              + " segundos de espera");
      return null;
    }
  }

  /**
   * Espera seg segundos a que element sea clicable.
   *
   * @param by localizador del elemento por el que esperamos
   * @param seg tiempo máximo de espera
   * @return devuelve element si es clicable, null en caso contrario
   */
  public WebElement waitForElementClickable(By by, int seg) {
    try {
      wait = new WebDriverWait(driver.getDriver(), seg);
      wait(by, seg);
      return wait.until(ExpectedConditions.elementToBeClickable(by));

    } catch (TimeoutException e) {
      log.error(
          "TimeOut. Elemento "
              + by
              + " no seleccionable después de "
              + seg
              + " segundos de espera");
      return null;
    }
  }

  /**
   * Click sobre element con validación y espera máxima de x segundos.
   *
   * @param element elemento sobre el que se realiza la acción
   * @param seg tiempo máximo de espera
   * @return true si hace click sobre element, false en caso contrario
   */
  public void click(WebElement element, int seg) {
    try {
      waitForElementClickable(element, seg);
      element.click();

    } catch (ElementNotSelectableException e) {
      log.error("No se ha podido realizar el click sobre " + element);
      throw e;
    }
  }

  /**
   * Hace click sobre element mediante la clase Action.
   *
   * @param element elemento sobre el que se realiza la acción
   */
  public void clickAction(WebElement element) {
    try {
      Actions action = new Actions(driver.getDriver());
      waitForElementClickable(element, shortWait);
      action.moveToElement(element).perform();
      action.click(element).perform();

    } catch (ElementNotSelectableException e) {
      log.error("No se ha podido realizar el Action click sobre " + element);
      throw e;
    }
  }

  /**
   * Hace doble click sobre element mediante la clase Action.
   *
   * @param element elemento sobre el que se realiza la acción
   */
  public void doubleClickAction(WebElement element) {
    try {
      Actions action = new Actions(driver.getDriver());
      waitForElementClickable(element, shortWait);
      action.moveToElement(element).perform();
      action.doubleClick(element).perform();

    } catch (ElementNotSelectableException e) {
      log.error("No se ha podido realizar el doble click sobre " + element);
      throw e;
    }
  }

  /**
   * Método para clicar y arrastrar un elemento
   *
   * @param source Elemento que se desea arrastrar
   * @param target Destino
   */
  public void dragAndDrop(WebElement source, WebElement target) {
    try {
      Actions action = new Actions(driver.getDriver());
      waitForElementClickable(source, shortWait);
      wait(target, shortWait);
      action.dragAndDrop(source, target).perform();

    } catch (RuntimeException e) {
      log.error("No se ha podido realizar la acción de drag&drop");
      throw e;
    }
  }

  /**
   * Hace click sobre element mediante javascriptExecutor.
   *
   * @param element webElement en el que deseamos clickear.
   * @return true si hace click con éxito sobre element, false en caso contrario.
   */
  public boolean clickJS(WebElement element) {
    try {
      waitForElementClickable(element, shortWait);
      ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
      return true;

    } catch (ElementNotSelectableException e) {
      log.error("No se ha podido realizar el JavascriptExecutor click sobre " + element);
      return false;
    }
  }

  /**
   * Limpia el texto de un campo imput y Escribe la cadena txt.
   *
   * @param element elemento sobre el que escribimos
   * @param txt texto a escribir
   */
  public void type(WebElement element, String txt) {
    try {
      waitForElementClickable(element, shortWait);
      element.clear();
      element.sendKeys(txt);

    } catch (NoSuchElementException e) {
      log.error("No se ha podido escribir la cadena \"" + txt + "\" sobre el elemento " + element);
      throw new RuntimeException(e);
    }
  }

  /**
   * Función que permite insertar un valor sobre un webElement de tipo Imput.
   *
   * @version 1.0 10/06/2019
   * @param element webElement en el que deseamos introducir un valor.
   * @param txt Valor que deseamos introducir en un webElement.
   */
  public void typeAction(WebElement element, String txt) {
    try {
      Actions action = new Actions(driver.getDriver());
      action.sendKeys(element, txt).perform();

    } catch (NoSuchElementException e) {
      log.error("No se ha podido escribir la cadena \"" + txt + "\" sobre el elemento " + element);
      throw new RuntimeException(e);
    }
  }

  /** Cambia al iframe por defecto */
  public void switchToDefault() {
    driver.getDriver().switchTo().defaultContent();
  }

  /**
   * Cambia al iframe dado por element
   *
   * @param element elemento que identifica el iframe
   * @param defaultFrame true si deseamos ir al iframe por defecto
   */
  public void switchToIframe(WebElement element, boolean defaultFrame) {
    try {
      if (defaultFrame) driver.getDriver().switchTo().defaultContent();
      wait(element, shortWait);
      driver.getDriver().switchTo().frame(element);
    } catch (RuntimeException e) {
      log.error(
          "Elemento "
              + element
              + " no encontrado. Tiempo máximo de espera: "
              + shortWait
              + " seg.");
      throw e;
    }
  }
}
