package utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import java.awt.*;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

/**
 * @description Clase que obtiene el SesionID de las cookies mostrado en el log
 * @version 1.0 11/04/2019
 */
public class GetSesionID {

  Configuration config = new Configuration();

  /**
   * @description Función que obtiene el SesionID de las cookies
   * @return
   */
  public void getSesionID(WebDriver driver) throws AWTException {

    Logger log = LogManager.getLogger(getClass().getName());
    //		PropertyConfigurator.configure(config.getLOG4J());

    Set<Cookie> cookies = driver.manage().getCookies();

    Iterator<Cookie> itr = cookies.iterator();
    String sessionId = null;

    while (itr.hasNext()) {
      Cookie cookie = itr.next();

      if (cookie.getName().compareTo("JSESSIONID") == 0) {
        sessionId = Optional.ofNullable(cookie.getValue()).orElse("No session ID");
      }
    }

    log.info("ID de sesión capturado");
    MDC.put("sessionId", sessionId);
  }
}
