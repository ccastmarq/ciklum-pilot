package utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class JSONMapper {
  private HashMap<String, Object> jsonMap;

  public JSONMapper(String jsonPath) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    jsonMap = objectMapper.readValue(new File(jsonPath),
        new TypeReference<HashMap<String,Object>>(){});
  }

  @SuppressWarnings("unchecked")
  public HashMap<String, Object> getJsonMap(String key) {
    return (HashMap<String, Object>)jsonMap.get(key);
  }
}