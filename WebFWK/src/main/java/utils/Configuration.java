package utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.io.*;
import java.util.Properties;
import java.lang.ClassLoader;

/**
 * Clase que tiene como objetivo efectuar la lectura de los parámetros incluidos en el fichero
 * "config.properties".
 */
public class Configuration {

  private Properties properties = new Properties();
  private Logger log;
  private String errorPrefix = "Campo ";
  private String errorSuffix = "no especificado en el archivo Configuration.properties.";

  /** Constructor. Realiza la lectura del fichero "config.properties". */
  public Configuration() {
    ClassLoader cl = Configuration.class.getClassLoader();
    //		log4j = cl.getResourceAsStream("log4j.properties");
    //		PropertyConfigurator.configure(log4j);
    log = LogManager.getLogger(getClass().getName());
    try {
      InputStream config = cl.getResourceAsStream("config.properties");
      properties.load(config);
    } catch (IOException e) {
      log.fatal("No se encuentra Configuration.properties", e);
    }
  }

  /**
   * Función que devuelve el navegador en el que queremos lanzar los test.
   *
   * @return browser
   */
  public String getBrowser() {
    String browser = properties.getProperty("Browser");
    if (browser != null) return browser;
    else {
      log.fatal(errorPrefix + "'Browser'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'Browser'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve true para ejecucion en Local y false para la ejecucion remota
   *
   * @return true or false
   */
  public boolean isRunLocally() {
    String Local = properties.getProperty("Local");
    if (Local != null && Local.contains("true")) {
      return true;
    } else if (Local != null && Local.contains("false")) {
      return false;
    } else {
      log.fatal(errorPrefix + "'Local'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'Local'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve la URL del servidor RemoteWebDriver
   *
   * @return
   */
  public String getRemoteDriver() {
    String url = properties.getProperty("remoteWebDriver");
    if (url != null) return url;
    else {
      log.fatal(errorPrefix + "'remoteWebDriver'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'remoteWebDriver'" + errorSuffix);
    }
  }

  /**
   * Función que obtiene ruta de acceso al driver del navegador Chrome.
   *
   * @return driverPath
   */
  public String getChromeDriver() {
    String driverPath = properties.getProperty("chromeDriverPath");
    if (driverPath != null) return driverPath;
    else {
      log.fatal(errorPrefix + "'chromeDriverPath'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'chromeDriverPath'" + errorSuffix);
    }
  }

  /**
   * Función que obtiene ruta de acceso al driver del navegador Firefox.
   *
   * @return driverPath
   */
  public String getFirefoxDriver() {
    String driverPath = properties.getProperty("firefoxDriverPath");
    if (driverPath != null) return driverPath;
    else {
      log.fatal(errorPrefix + "'firefoxDriverPath'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'firefoxDriverPath'" + errorSuffix);
    }
  }

  /**
   * Función que obtiene ruta de acceso al driver del navegador Internet Explorer.
   *
   * @return driverPath
   */
  public String getIEDriver() {
    String driverPath = properties.getProperty("iexplorerDriverPath");
    if (driverPath != null) return driverPath;
    else {
      log.fatal(errorPrefix + "'iexplorerDriverPath'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'iexplorerDriverPath'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve los tiempos de espera indicadas para errores de carga en las aplicaciones
   * web.(tiempo mayor)
   *
   * @return Integer.parseInt(longWebDriverWait)
   */
  public int getLongWebDriverWait() {
    String waitWebDriverLongTime = properties.getProperty("longWebDriverWait");
    if (waitWebDriverLongTime != null) return Integer.parseInt(waitWebDriverLongTime);
    else {
      log.fatal(errorPrefix + "'longWebDriverWait'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'longWebDriverWait'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve los tiempos de espera indicadas para errores de carga en las aplicaciones
   * web.(tiempo mucho menor)
   *
   * @return Integer.parseInt(shortWebDriverWait)
   */
  public int getShortWebDriverWait() {
    String wait = properties.getProperty("shortWebDriverWait");
    if (wait != null) return Integer.parseInt(wait);
    else {
      log.fatal(errorPrefix + "'shortWebDriverWait'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'shortWebDriverWait'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve los tiempos de espera indicadas para errores de carga en las aplicaciones
   * web.(tiempo mayor).
   *
   * @return Integer.parseInt(loadTimeOut)
   */
  public int getLoadTimeOut() {
    String loadTimeOut = properties.getProperty("loadTimeOut");
    if (loadTimeOut != null) return Integer.parseInt(loadTimeOut);
    else {
      log.fatal(errorPrefix + "'loadTimeOut'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'loadTimeOut'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve la ruta donde se guardan los screenshots
   *
   * @return
   */
  public String getScreenshotDir() {
    String dir = properties.getProperty("screenshots");
    if (dir != null) return dir;
    else {
      log.fatal(errorPrefix + "'screenshots'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'screenshots'" + errorSuffix);
    }
  }

  /**
   * Función que devuelve la ruta donde se guardan los screenshots
   *
   * @return
   */
  public String getEvidencesDir() {
    String dir = properties.getProperty("zippedEvidences");
    if (dir != null) return dir;
    else {
      log.fatal(errorPrefix + "'zippedEvidences'" + errorSuffix);
      throw new RuntimeException(errorPrefix + "'zippedEvidences'" + errorSuffix);
    }
  }
}
