package utils;

import driver.WebFWK;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Clase que permite agrupar aquellas funciones que son reutilizables para diversos proyectos de
 * automatización con Selenium.
 *
 * @author TEAM QA AUTOMATION ATMIRA
 */
public class Functions {

  public static Date date;
  public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
  private static Logger log = LogManager.getLogger(Functions.class.getName());
  private static Configuration config = new Configuration();
  private static long startTime;
  private static long endTime;

  /** Constructor */
  public Functions() {
    date = new Date();
  }

  /** Establece el tiempo de inicio del test */
  public static void setStartTime() {
    Functions.startTime = System.currentTimeMillis();
  }

  /** Establece el tiempo de finalizacion del test */
  public static void setEndTime() {
    Functions.endTime = System.currentTimeMillis();
    ;
  }

  /**
   * Función que permite devolver el nombre de la función desde la que es llamada esta función.
   *
   * @return nombre del metodo desde el cual se hace el llamado
   */
  public static String getNameMethod() {
    return new Exception().getStackTrace()[1].getMethodName();
  }

  /**
   * Limpia el contenido del directorio dir
   *
   * @param dir directorio a limpiar
   * @throws IOException Error de lectura de fichero
   */
  public static void clearFileContent(String dir) throws IOException {
    File file = new File(dir);
    if (file.exists()) {
      try {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(""); // borrar contenido fichero
        bw.close();
      } catch (IOException error) {
        log.error("*** ERROR *** - Tratamiento de Fichero Log. ");
        log.error("*** FOUND EXCENTION *** " + error.getMessage());
        error.printStackTrace();
        throw (error);
      }
    }
  }

  /**
   * Limpia el contenido de un directorio
   *
   * @param dirPath ruta del directorio
   * @throws IOException error de lectura de fichero
   */
  public static void clearDirectory(String dirPath) throws IOException {
    File directorio = new File(dirPath);
    try {
      FileUtils.cleanDirectory(directorio);
      // log.info("Eliminadas capturas de pantalla existentes en " + dirPath);
    } catch (IOException IOEx) {
      IOEx.printStackTrace();
      log.error("*** ERROR *** Error al borrar el directorio " + dirPath);
      throw (IOEx);
    }
  }

  /**
   * Crea un directorio
   *
   * @param dirPath ruta destino del directorio
   */
  public static void createDirectory(String dirPath) {
    File dir = new File(dirPath);
    if (!dir.exists()) {
      if (dir.mkdir()) {
        log.info("Creada carpeta " + dirPath);
      } else {
        log.fatal("Error creando carpeta " + dirPath);
        throw new RuntimeException("Error creando carpeta " + dirPath);
      }
    }
  }

  /**
   * Funcion que copia el archivo source en la ruta de destino target
   *
   * @param source archivo fuente
   * @param target archivo destino
   */
  public static void copyFile(String source, String target) {
    File origin = new File(source);
    File destination = new File(target + File.separator + origin.getName());

    if (origin.exists()) {
      try {
        FileUtils.copyFile(origin, destination);
        log.info("Archivo " + source + " copiado en " + target);
      } catch (IOException e) {
        log.fatal("Error al copiar el archivo: " + e);
      }
    } else {
      log.error("Archivo de origen " + source + " no existe");
    }
  }

  /**
   * Copia el contenido del fichero source en el fichero de destino target
   *
   * @param source fichero origen
   * @param target fichero destino
   * @throws IOException fallo de lectura de fichero
   */
  public static void copyFileContent(String source, String target) throws IOException {
    File origin = new File(source);
    File destination = new File(target);
    String missingFile = "Fichero de origen " + source + " o destino " + target + " no exite";
    if (origin.exists()) {
      try {
        InputStream in = new FileInputStream(origin);
        OutputStream out = new FileOutputStream(destination);
        // Usamos un buffer para la copia.
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        in.close();
        out.close();
        log.info("Fichero " + source + " copiado con éxito en la ruta " + target);
      } catch (IOException IOEx) {
        IOEx.printStackTrace();
        log.fatal("No se ha podido copiar el fichero " + source + " en la ruta " + target);
      }
    } else {
      log.fatal(missingFile);
      throw new IOException(missingFile);
    }
  }

  /**
   * Función que renombra un archivo
   *
   * @param from archivo a renombrar
   * @param to nuevo nombre
   */
  public static void renameFile(String from, String to) {
    File before = new File(from);
    File after = new File(to);
    if (before.renameTo(after)) log.info("Archivo " + from + " renombrado a " + to);
    else {
      log.error("No se ha podido renombrar el archhivo " + from);
      throw new RuntimeException("No se ha podido renombrar el archhivo " + from);
    }
  }

  /**
   * Comprime un directorio
   *
   * @param dirInput fichero origen
   * @param nameZIP ruta fichero destino
   * @throws IOException fallo de lectura de fichero
   */
  public static void zipDirectory(String dirInput, String nameZIP) throws IOException {

    FileOutputStream fos = new FileOutputStream(nameZIP);
    ZipOutputStream zipOut = new ZipOutputStream(fos);
    File fileToZip = new File(dirInput);

    zipFile(fileToZip, fileToZip.getName(), zipOut);
    zipOut.close();
    fos.close();
  }

  /**
   * Comprime un fichero
   *
   * @param fileToZip fichero a comprimir
   * @param fileName nombre final del fichero a comprimir
   * @param zipOut flujo de salida
   * @throws IOException fallo de lectura de fichero
   */
  private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut)
      throws IOException {
    if (fileToZip.isHidden()) {
      return;
    }

    if (fileToZip.isDirectory()) {
      if (fileName.endsWith("/")) zipOut.putNextEntry(new ZipEntry(fileName));
      else zipOut.putNextEntry(new ZipEntry(fileName + "/"));

      zipOut.closeEntry();

      File[] children = fileToZip.listFiles();
      for (File childFile : children)
        zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);

      return;
    }
    FileInputStream fis = new FileInputStream(fileToZip);
    ZipEntry zipEntry = new ZipEntry(fileName);
    zipOut.putNextEntry(zipEntry);
    byte[] bytes = new byte[1024];
    int length;
    while ((length = fis.read(bytes)) >= 0) {
      zipOut.write(bytes, 0, length);
    }
    fis.close();
  }

  /**
   * Función que permite efectuar capturas de pantalla creada para IExplorer, no usada.
   *
   * @param driver Driver de la pagina a la que se le va a realizar la captura
   * @param idTest Identificador del test para el nombre de la captura
   * @throws IOException Error dentro de las capturas de pantalla
   */
  public static void takeScreenshot(WebFWK driver, String idTest) throws IOException {

    TakesScreenshot scrShot = ((TakesScreenshot) driver.getDriver());
    String snapshotDir;
    File dir;
    date = new Date();
    SimpleDateFormat timeStamp = new SimpleDateFormat("dd-MM-yyyy_HHmmss");

    try {
      snapshotDir =
          config.getScreenshotDir()
              + File.separator
              + dateFormat.format(date)
              + "_"
              + idTest
              + File.separator;
      dir = new File(snapshotDir);
    } catch (RuntimeException error) {
      log.error("Ruta de capturas inválida");
      throw error;
    }

    if (!dir.exists()) {
      if (dir.mkdir()) log.info("Creada carpeta " + dir.getPath());
      else log.error("Error al crear carpeta de evidencias" + dir.getPath());
    }

    try {
      // Call getScreenshotAs method to create image file
      File screenshotFile = scrShot.getScreenshotAs(OutputType.FILE);

      // Move image file to new destination
      String snapshot = idTest + " - " + timeStamp.format(date) + ".png";
      File destFile = new File(snapshotDir + snapshot);

      // Copy file at destination
      FileUtils.copyFile(screenshotFile, destFile);

    } catch (IOException e) {
      log.error("Error al crear captura");
      throw e;
    }
  }

  /**
   * Función que devuelve la duración del test en formato 'x min, y seg, z miliseg'
   *
   * @return duracion del test en formato min, seg, miliseg
   */
  public static String testDuration() {
    long durationTime = (endTime - startTime);

    long milSegTime = durationTime;
    long minTime = (durationTime / 1000) / 60;
    long segTime = (durationTime / 1000) % 60;

    if (durationTime > 1000) {
      milSegTime = durationTime % 1000;
    }

    return String.format("%d min, %d seg, %d miliseg.", minTime, segTime, milSegTime);
  }

  /**
   * Devuelve el nombre del archivo .feature
   *
   * @param str id completo del feature
   * @return nombre del feature
   */
  public static String getFeature(String str) {
    String[] aux1 = str.split("\\.");
    String aux = aux1[aux1.length - 2];
    String[] array = aux.split("/");
    return array[array.length - 1];
  }
}
