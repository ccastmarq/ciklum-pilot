package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.Configuration;

public class WebFWK {

  private static Logger log = LogManager.getLogger(WebFWK.class.getName());
  private Configuration config = new Configuration();
  // private WebDriver driver;
  private RemoteWebDriver driver;

  /** Constructor. Inicia el driver con el browser seleccionado en el properties */
  public WebFWK() {
    try {
      boolean isRunLocally = config.isRunLocally();
      String browser = config.getBrowser();

      if (isRunLocally) localDriver(browser);
      else remoteDriver(browser);

      driver.manage().deleteAllCookies(); // Eliminar cookies
      driver.manage().window().maximize(); // Ventana Maximizada

      MDC.put("sessionId", ""); // limpiar Sesion ID

      log.info("");
      log.info("\t INICIALIZANDO DRIVER");

      Capabilities caps = driver.getCapabilities();
      log.info(
          "Configuracion basica --- Navegador: "
              + caps.getBrowserName().toUpperCase()
              + " "
              + caps.getVersion()
              + " - Resolucion: "
              + driver.manage().window().getSize().getWidth()
              + "x"
              + driver.manage().window().getSize().getHeight()); // toLowerCase
    } catch (IOException e) {
      log.fatal("Error al levantar AtmiraWebDriver");
      e.printStackTrace();
    }
  }

  /**
   * Inicializa un driver para ejecutar en local
   *
   * @param browser Navegador a ejecutar
   */
  private void localDriver(String browser) {
    switch (browser) {
      case "chrome":
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        break;

      case "firefox":
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
        break;

      case "iexplorer":
        WebDriverManager.iedriver().setup();
        driver = new InternetExplorerDriver();
        break;

      default:
        log.fatal(
            "Navegador indicado en config.properties incorrecto. Indique firefox, iexplorer o chrome");
        break;
    }

    driver.manage().timeouts().pageLoadTimeout(config.getLoadTimeOut(), TimeUnit.SECONDS);
  }

  /**
   * Inicializa un driver para ejecutar de forma remota
   *
   * @param browser Navegador a ejecutar
   * @throws MalformedURLException Error en el formato de la URL
   */
  private void remoteDriver(String browser) throws MalformedURLException {
    DesiredCapabilities capabilities = null;
    switch (browser) {
      case "chrome":
        capabilities = DesiredCapabilities.chrome();
        break;

      case "firefox":
        capabilities = DesiredCapabilities.firefox();
        break;

      case "iexplorer":
        capabilities = DesiredCapabilities.internetExplorer();
        break;
      default:
        log.fatal(
            "Navegador indicado en config.properties incorrecto. Indique firefox, iexplorer o chrome");
        break;
    }

    driver = new RemoteWebDriver(new URL(config.getRemoteDriver()), capabilities);
    LocalFileDetector detector = new LocalFileDetector();
    driver.setFileDetector(detector);
    driver.manage().timeouts().pageLoadTimeout(config.getLoadTimeOut(), TimeUnit.SECONDS);
  }

  /**
   * Funcion que devuelve el driver de selenium
   *
   * @return selenium RemoteWebDriver
   */
  public WebDriver getDriver() {
    return driver;
  }

  /** Función que cierra el driver */
  public void exit() {
    this.driver.quit();
  }

  /**
   * Funcion para navegar a una URL
   *
   * @param url URL a la que navegar
   */
  public void navigateTo(String url) {
    driver.get(url);
  }

  /**
   * Comprueba si se muestra una ventana emergente
   *
   * @return true si se muestra una ventana emergente
   */
  public boolean isAlertPresent() {
    try {
      Thread.sleep(1000);
      driver.switchTo().alert();
      return true;

    } catch (NoAlertPresentException | InterruptedException Ex) {
      return false;
    }
  }

  /**
   * Acepta un mensaje de alerta emergente
   *
   * @return Mensaje de alerta
   */
  public String acceptAlert() {
    Alert alert = driver.switchTo().alert();
    String alertMsg;

    driver.switchTo().alert();
    alertMsg = alert.getText();
    alert.accept();

    return alertMsg;
  }

  /**
   * Rechaza un mensaje de alerta emergente
   *
   * @return Mensaje de alerta
   */
  public String dimissAlert() {
    Alert alert = driver.switchTo().alert();
    String alertMsg;

    driver.switchTo().alert();
    alertMsg = alert.getText();
    alert.dismiss();

    return alertMsg;
  }

  /**
   * Funcion que permite ejecutar un javascript
   *
   * @param script script a ejecutar
   * @return
   */
  private Object executeJavascript(String script) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    return js.executeScript(script);
  }

  /**
   * Funcion que realiza un scroll hacia un element mediante javascript
   *
   * @param element elemento hacia al que hacer scroll
   */
  public void srollToElement(WebElement element) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].scrollIntoView();", element);
  }

  /**
   * Fucnion que hace scroll hacia un par de coordenadas
   *
   * @param x coordenada horizontal
   * @param y coordenada vertical
   */
  public void srollToXY(int x, int y) {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(" + x + "," + y + ")");
  }

  /**
   * Funcion que abre un enlace en otra ventana
   *
   * @param element elemento con link para abrir
   */
  public WebFWK openLinkNewWindow(WebElement element) {

    int beforeOpen = driver.getWindowHandles().size();
    String link = element.getAttribute("href");
    String js = "window.open(\"" + link + "\");";
    String focus = "window.focus();";
    executeJavascript(js);
    executeJavascript(focus);
    log.debug("Antes = " + beforeOpen);
    while (driver.getWindowHandles().size() == beforeOpen) {
      try {
        driver.wait(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    int afterOpen = driver.getWindowHandles().size();
    String[] s = driver.getWindowHandles().toArray(new String[afterOpen]);
    driver.switchTo().window(s[s.length - 1]);

    return this;
  }

  /**
   * Funcion que hace foco en la ventana cuyo titulo exacto sea el indicado
   *
   * @param title titulo exacto de la ventana
   * @return boolean indicando el éxito de la operación
   */
  public boolean selectWindow(String title) {
    Set<String> ids = driver.getWindowHandles();
    for (String id : ids) {
      driver.switchTo().window(id);
      if (driver.getTitle().equals(title)) {
        log.debug("Cambiado a ventana " + title);
        return true;
      }
    }
    return false;
  }

  /**
   * Funcion que hace foco en la ventana cuyo título contenga el indicado
   *
   * @param title parte del titulo a buscar
   * @return boolean indicando el éxito de la operación
   */
  public boolean selectWindowContaningTitle(String title) {
    Set<String> ids = driver.getWindowHandles();
    for (String id : ids) {
      driver.switchTo().window(id);
      if (driver.getTitle().contains(title)) {
        log.debug("Cambiado a ventana " + title);
        return true;
      }
    }
    return false;
  }

  /**
   * Método para navegar dentro de un Shadow-root
   *
   * @param element raíz del shadow-root
   * @return Devuelve el elemento Shadow-root que cuelga de element
   */
  public WebElement expandRootElement(WebElement element) {
    return (WebElement)
        ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot", element);
  }
}
