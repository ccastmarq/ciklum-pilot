package projectdata;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
@description Clase que tiene como objetivo efectuar la lectura de los parámetros incluidos en el fichero "projectData.properties".
@version 1.1 11/03/2020
 */
public class ProjectDataReader {

	private Properties properties = new Properties();
	private Logger log;
	private String fileNane ="projectData.properties";
	private String errorPrefix = "Campo ";
	private String errorSuffix = " no especificado en el archivo " + fileNane;

	/**
	 * Constructor
	 */
	public ProjectDataReader() {
		InputStream envData = ProjectDataReader.class.getClassLoader().getResourceAsStream(fileNane);
		log = LogManager.getLogger(getClass().getName());
		try {
			properties.load(envData);
		} catch (IOException e) {
			log.fatal("No se encuentra " + fileNane, e);
		}
	}

	/**
	 * Función que obtiene ruta de acceso al Fichero de Log.
	 * @return rutaFichero
	 */
	public String getTestLogPath() {
		String field = "testLog";
		String aux = properties.getProperty(field);
		if (aux != null)
			return aux;
		else{
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

	/**
	 * Función que obtiene ruta de acceso al Fichero de Log.
	 * @return rutaFichero
	 */
	public String getExecTestLogPath() {
		String field = "executionLog";
		String aux = properties.getProperty(field);
		if (aux != null)
			return aux;
		else{
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

	/**
	 * Función que devuelve el entorno
	 * @return Environment
	 */
	public String getEnv() {
		String field = "environment";
		String aux = properties.getProperty(field);

		if (aux != null)
			return aux;
		else{
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

	public String getURL() {
		String field = "url";
		String aux = properties.getProperty(field);

		if (aux != null)
			return aux;
		else {
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

	public String getUser() {
		String field = "user";
		String aux = properties.getProperty(field);

		if (aux != null)
			return aux;
		else {
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

	public String getPass() {
		String field = "password";
		String aux = properties.getProperty(field);

		if (aux != null)
			return aux;
		else {
			log.fatal(errorPrefix + "'" + field +"'" + errorSuffix);
			throw new RuntimeException(errorPrefix + "'" + field +"'" + errorSuffix);
		}
	}

}