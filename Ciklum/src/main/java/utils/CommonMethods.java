package utils;

import driver.WebFWK;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;

public class CommonMethods {

    private static Logger log = LogManager.getLogger(Functions.class.getName());

    public void fillInputsFromMap (WebFWK driver, HashMap<String, String> map) {

        for(HashMap.Entry<String, String> entry : map.entrySet()){
            if(entry.getKey().startsWith("select-")){
                Select office = new Select(driver.getDriver().findElement(
                        By.id(entry.getKey().split("select-")[1])));
                office.selectByValue(entry.getValue());
            } else {
                WebElement el = driver.getDriver().findElement(By.id(entry.getKey()));
                el.sendKeys(entry.getValue());
            }
        }
    }
}