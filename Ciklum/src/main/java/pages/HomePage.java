package pages;

import driver.WebFWK;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (id = "uc-banner-text")
    private WebElement cookiesBanner;

    @FindBy (id = "uc-btn-accept-banner")
    private WebElement acceptCookiesBtn;

    @FindBy (className = "stage__overlay")
    private WebElement stageOverlay;

    @FindBy (xpath = "//*[@class='signpost--wrap col-xs-6']")
    private List<WebElement> categories;


//  ----------------------------------
//              Constructor
//  ----------------------------------
    public HomePage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(cookiesBanner, shortWait) !=null;
    }

    public boolean areCookiesAccepted(){
        return wait(stageOverlay, shortWait) !=null;
    }

    public void acceptCookies() {
        wait(acceptCookiesBtn, shortWait);
        click(acceptCookiesBtn, shortWait);
    }

    public void selectFirstCategory() {
        wait(categories.get(0), shortWait);
        click(categories.get(0), shortWait);
    }
}
