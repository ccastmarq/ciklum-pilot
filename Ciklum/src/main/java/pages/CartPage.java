package pages;

import driver.WebFWK;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class CartPage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy(xpath = "//*[@class='progress-bar__icon icon icon--till']")
    private WebElement cartProgressIcon;

    @FindBy (xpath = "//*[@class='cart-item--img']")
    private List<WebElement> cartItems;

    @FindBy (className = "cart--header__icons")
    private WebElement cartBtn;

    @FindBy (className = "cart__sum__voucher-link")
    private WebElement redeemBtn;

    @FindBy (name = "voucherCode")
    private WebElement voucherInput;

    @FindBy (xpath = "//button[normalize-space()='redeem']")
    private WebElement redemVoucher;

    @FindBy (className = "cart__sum-voucher-delete-link")
    private WebElement removeVoucherBtn;

//  ----------------------------------
//              Constructor
//  ----------------------------------
    public CartPage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(cartProgressIcon, shortWait) != null;
    }

    public boolean isEmpty() {
        return cartItems.isEmpty();
    }

    public boolean isVoucherApplied() {
        return wait(removeVoucherBtn, shortWait) != null;
    }

    public void applyVoucher(String code) {
        click(redeemBtn, shortWait);
        click(voucherInput, shortWait);
        voucherInput.sendKeys(code);
        click(redemVoucher, shortWait);
    }

    public void goToCart() {
        wait(cartBtn, shortWait);
        click(cartBtn, shortWait);
    }
}
