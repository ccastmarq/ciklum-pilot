package pages;

import driver.WebFWK;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class HandbagsPage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (xpath = "//*[@data-category='Handbags']")
    private WebElement handbagSection;

    @FindBy (className = "product--list__item__image")
    private List<WebElement> products;


//  ----------------------------------
//              Constructor
//  ----------------------------------
    public HandbagsPage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(handbagSection, shortWait) !=null;
    }

    public void selectFirstProduct(){
        wait(products.get(0), shortWait);
        click(products.get(0), shortWait);
    }
}
