package pages;

import driver.WebFWK;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class TopHeader extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (className = "header__top")
    private WebElement header;

    @FindBy (xpath = "//*[@class='icon icon--user']")
    private WebElement userIcon;


//  ----------------------------------
//              Constructor
//  ----------------------------------
    public TopHeader(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(header, shortWait) != null;
    }


    public void login() {
        click(userIcon, shortWait);
    }

}
