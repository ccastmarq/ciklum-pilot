package pages;

import driver.WebFWK;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ProductPage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (xpath = "//*[@class='flex-center-content product-details__add-to-cart-container']")
    private WebElement addToCartBtn;

    @FindBy (xpath = "//*[@class='cart--header']")
    private WebElement cartHeader;



//  ----------------------------------
//              Constructor
//  ----------------------------------
    public ProductPage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(addToCartBtn, shortWait) != null;
    }

    public boolean isAddedToCart() {
        wait(cartHeader, shortWait);
        return waitForElementClickable(cartHeader, shortWait) != null;
    }

    public void addToCart() {
        wait(addToCartBtn, shortWait);
        click(addToCartBtn, shortWait);
    }

    public void openCart() {
        wait(cartHeader, shortWait);
        click(cartHeader, shortWait);
    }
}
