package pages;

import driver.WebFWK;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class UserPage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (className = "account__firstname")
    private WebElement loginMessage;

    @FindBy (xpath = "//a[normalize-space()='Personal data']")
    private WebElement personalData;

    @FindBy (className = "account--address__action--update")
    private WebElement editAddressBtn;

    @FindBy (name = "firstName")
    private WebElement firstNameInput;

    @FindBy (name = "lastName")
    private WebElement lastNameInput;

    @FindBy (className = "account--address__action--save")
    private WebElement saveAddressBtn;

//  ----------------------------------
//              Constructor
//  ----------------------------------
    public UserPage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLogged() {
        return wait(loginMessage, shortWait) != null;
    }

    public void updateUserName(String name, String surName) {
        click(personalData, shortWait);
        click(editAddressBtn, shortWait);
        click(firstNameInput, shortWait);
        firstNameInput.clear();
        firstNameInput.sendKeys(name);
        click(lastNameInput, shortWait);
        lastNameInput.clear();
        lastNameInput.sendKeys(surName);
        click(saveAddressBtn, shortWait);
    }

    public boolean isUserNameUpdated(String name, String surName) {
        By by = new By.ByXPath("//div[@class='account__content__column " +
                "account__content__column--customer' and contains(., '" + name + " " + surName + "')]");
         return wait(by, shortWait) != null;
    }
}
