package pages;

import driver.WebFWK;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends AbstractPageObject{

//  ----------------------------------
//              WebElements
//  ----------------------------------
    @FindBy (name = "email")
    private WebElement emailInput;

    @FindBy (name = "password")
    private WebElement passwordInput;

    @FindBy (xpath = "//*[@class='text__center' and normalize-space()='Welcome to fashionette!']")
    private WebElement welcomeTitle;

    @FindBy (xpath = "//*[@type='submit' and preceding-sibling::div[normalize-space()='Login']]")
    private WebElement loginBtn;

//  ----------------------------------
//              Constructor
//  ----------------------------------
    public LoginPage(WebFWK driver){
        super(driver);
    }

//  ----------------------------------
//              Methods
//  ----------------------------------
    public boolean isLoaded(){
        return wait(welcomeTitle, shortWait) != null;
    }

    public void signIn(String user, String password) {
        type(emailInput, user);
        type(passwordInput, password);
        click(loginBtn, shortWait);
    }

}
