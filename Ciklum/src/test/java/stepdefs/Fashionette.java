package stepdefs;

import driver.WebFWK;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.IOException;
import org.testng.Assert;
import pages.*;
import projectdata.ProjectDataReader;
import utils.Functions;


public class Fashionette {
    private WebFWK driver;
    private String feature;
    private ProjectDataReader properties;
    //Page Objects
    private HomePage homePage;
    private TopHeader topHeader;
    private LoginPage login;
    private UserPage user;
    private HandbagsPage handbags;
    private ProductPage product;
    private CartPage cart;
    //Variables
    private String name = "Tester";
    private String surName = "Selenium";

    @Before()
    public void startUp (Scenario scenario) {
        driver = new WebFWK();
        properties = new ProjectDataReader();
        //Page Objects
        homePage = new HomePage(driver);
        topHeader = new TopHeader(driver);
        login = new LoginPage(driver);
        user = new UserPage(driver);
        handbags = new HandbagsPage(driver);
        product = new ProductPage(driver);
        cart = new CartPage(driver);
        this.feature = Functions.getFeature(scenario.getId()) + "_" + scenario.getName();
        Functions.setStartTime(); //iniciar cronometro
    }

    @Given("User navigates to {string} home page")
    public void logIn (String url) throws IOException {
        driver.navigateTo(url);
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(homePage.isLoaded());
    }

    @And("accepts cookies")
    public void cookies() {
        homePage.acceptCookies();
    }

    @Then("home page is visible")
    public void homePage() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(homePage.areCookiesAccepted());
    }

    @When("user navigates to a product")
    public void goToProduct() throws IOException {
        homePage.selectFirstCategory();
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(handbags.isLoaded());
        handbags.selectFirstProduct();
    }

    @Then("product pages is displayed")
    public void checkProductPage() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(product.isLoaded());
    }


    @When("user adds a product to the cart")
    public void addProductToCart() {
        product.addToCart();
    }

    @Then("product is successfully added to the cart")
    public void checkProductInCart() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(product.isAddedToCart());
    }

    @When("user {string} logs in with credentials")
    public void login(String user) throws IOException {
        topHeader.login();
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(login.isLoaded());
        login.signIn(user, properties.getPass());
    }

    @Then("user signs in correctly")
    public void checkLogin() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(user.isLogged());
    }

    @When("user visits the cart")
    public void goToCart() {
        cart.goToCart();
    }

    @Then("cart page loads successfully")
    public void checkCartPage() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(cart.isLoaded());
    }

    @When("user modifies the user information")
    public void modifyUserInfo() throws IOException {
        user.updateUserName(name, surName);
    }

    @Then("changes are correctly saved")
    public void verifyChanges() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(user.isUserNameUpdated(name, surName));
    }

    @When("users applies {string} voucher to the cart")
    public void applyVoucher(String voucher) throws IOException {
        cart.goToCart();
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(cart.isLoaded());
        cart.applyVoucher(voucher);
    }

    @Then("voucher is properly applied")
    public void checkVoucher() throws IOException {
        Functions.takeScreenshot(driver, feature);
        Assert.assertTrue(cart.isVoucherApplied());
    }


    @After()
    public void tearDown() {
        driver.exit();
    }
}