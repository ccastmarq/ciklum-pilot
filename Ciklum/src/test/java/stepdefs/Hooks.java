package stepdefs;

import projectdata.ProjectDataReader;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import utils.Configuration;
import utils.Functions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hooks {

    private ProjectDataReader envData = new ProjectDataReader();
    private Configuration config = new Configuration();
    private Logger log = LogManager.getLogger(Hooks.class.getName());

    private final String environment= envData.getEnv();
    private final String execLogPath = envData.getExecTestLogPath();
    private final String testLogPath = envData.getTestLogPath();

    private Date date = new Date(); //Actualizar fecha/hora de ejecucion para generar carpeta con capturas
    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private String startDate = dateFormat.format(date);;

    private String snapshotsPath = "." + File.separator + config.getScreenshotDir();
    private String historyPath = "." + File.separator + config.getEvidencesDir();
    private String testSnapsPath;


    @Before(value = "@first", order=10)
    public void cleanProofFolder() throws IOException {
        log.info("");
        log.info("\t LIMPIANDO DIRECTORIOS DE EVIDENCIAS ");
        log.info("");

        File snapFiles = new File(snapshotsPath);
        File snapHistoryFile = new File(historyPath);

        if (snapFiles.exists()) {
            Functions.clearDirectory(snapshotsPath);
        }

        Functions.createDirectory(snapshotsPath);

        if (!snapHistoryFile.exists()) {
            Functions.createDirectory(historyPath);
        }
    }

    @Before()
    public void prepareTestEvidencesFolder(Scenario scenario) throws IOException {
        Functions.clearFileContent(testLogPath);

        testSnapsPath = snapshotsPath + File.separator + dateFormat.format(date)
                + "_" + Functions.getFeature(scenario.getId())+ "_" + scenario.getName();
        Functions.createDirectory(testSnapsPath);
    }

    @After()
    public void finishTest(Scenario scenario) throws IOException {
        Functions.setEndTime(); //parar cronometro

        log.info("=======================================================================================================================================");
        log.info("  ***   Caso de Prueba con IDTest: " + Functions.getFeature(scenario.getId()) + "   ***");
        log.info("       +  Scenario: " + scenario.getName());
        log.info("       +  Entornodo de pruebas: " + environment);
        log.info("       +  Directorio con evidencias del Test: " + testSnapsPath);
        log.info("       +  Historico con evidencias de los Test: " + historyPath);
        log.info("       +  Tiempo ejecucion del Test: " + Functions.testDuration());
        log.info("       +  Resultado Final del Test:  [ "  + scenario.getStatus().toString().toUpperCase() + " ]");
        log.info("=======================================================================================================================================");

        Functions.copyFile(testLogPath, testSnapsPath);
    }

    @After("@last")
    public void zipProofDir() throws IOException, InterruptedException {
       Functions.copyFile(execLogPath, snapshotsPath);

        log.info("");
        log.info("\t COMPRIMIENDO DIRECTORIOS DE EVIDENCIAS ");
        log.info("");

        try{
            Functions.zipDirectory(snapshotsPath, "evidences.zip");
            Thread.sleep(1000);
            Functions.copyFileContent( "." + File.separator + "evidences.zip",
                    historyPath + File.separator + "evidences_" + startDate + "_" + environment +  ".zip");
            Thread.sleep(1000);
            FileUtils.deleteQuietly(new File("evidences.zip"));
            log.info("Se ha creado el zip de evidencias <" + "evidences_" + startDate + "_" + environment +  ".zip>"
                    + " en la ruta " + historyPath);
        } catch (IOException | InterruptedException e) {
            log.error("Error al comprimir evidencias");
            e.printStackTrace();
            throw e;
        }
    }
}