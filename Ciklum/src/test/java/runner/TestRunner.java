package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(strict= true,
		plugin = {"pretty", "json:target/report/cucumber.json"},
		glue = {"stepdefs"},
		features = { "src/test/resources/features"}
		)
public class TestRunner extends AbstractTestNGCucumberTests {

}
