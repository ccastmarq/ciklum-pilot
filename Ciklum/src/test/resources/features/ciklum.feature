Feature: Ciklum Pilot
  Background:
    Given User navigates to 'http://www.fashionette.co.uk' home page
    And accepts cookies
    Then home page is visible

  @first
  Scenario: 01 - Add a product to the cart and login
    When user navigates to a product
    Then product pages is displayed
    When user adds a product to the cart
    Then product is successfully added to the cart
    When user 'QA@fashionette.de' logs in with credentials
    Then user signs in correctly
    When user visits the cart
    Then cart page loads successfully

  Scenario: 02 - Modify user information
    When user 'QA@fashionette.de' logs in with credentials
    Then user signs in correctly
    When user modifies the user information
    Then changes are correctly saved

  @last
  Scenario: 03 - Apply a voucher
    When user navigates to a product
    Then product pages is displayed
    When user adds a product to the cart
    Then product is successfully added to the cart
    When users applies 'qachallenge' voucher to the cart
    Then voucher is properly applied