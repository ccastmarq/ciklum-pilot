# Ciklum Pilot
## Getting Started

This solution is based on a Selenium framework with Cucumber.

### Prerequisites

Apache maven and latest java 8 JRE must be installed.

### Installation

1. Clone the repo:
   ```sh
   git clone https://ccastmarq@bitbucket.org/ccastmarq/ciklum-pilot.git
   ```
   
2. Install WebFMW with following command within WebBMW\pom.xml path:
   ```sh
   mvn clean install
   ```
   
3. Run tests with following comman within Ciklum\pom.xml path:
   ```sh
   mvn clean test
   ```
 
### Test results
1. Screenshot are saved in Ciklum\screenshots.

2. In history folder there will be a ZIP file of these screenshots.

3. Use following command to generate an HTML report file:
   ```sh
   mvn cluecumber-report:reporting
   ```

4. HTML file will be generated in Ciklum\target\generated-report\index.html. Open it with any broswer.
